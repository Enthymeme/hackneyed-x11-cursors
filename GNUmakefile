# Copyright (C) Richard Ferreira
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written authorization.

.DEFAULT_GOAL := all-dist
DESTDIR ?= /
PREFIX ?= $(DESTDIR)usr/local
VERSION = 0.9.3
INKSCAPE ?= /usr/bin/inkscape
INSTALL ?= /usr/bin/install
JQ ?= /usr/bin/jq

SIZE_SMALL := 24
SIZE_MEDIUM := 36
SIZE_LARGE := 48
SIZE_LARGE1 := 60
SIZE_LARGE2 := 72
SIZE_LARGE3 := 84
SIZE_LARGE4 := 96
# come to the dark side, we have hourglasses
ifeq ($(DARK_THEME), 1)
    $(info ----------- Using "dark" sources -------------)
    TWOHANDED_SOURCE := theme/common-dark
    RSVG_SOURCE := theme/right-handed-dark
    LSVG_SOURCE := theme/left-handed-dark
    WINDOWS_SOURCE := theme/windows-large-dark.svg
    THEME_NAME := Hackneyed-Dark
    THEME_COMMENT := Windows 3.x-inspired cursors (dark variant)
else
    $(info ----------- Using "light" sources ------------)
    TWOHANDED_SOURCE := theme/common-light
    RSVG_SOURCE := theme/right-handed-light
    LSVG_SOURCE := theme/left-handed-light
    WINDOWS_SOURCE := theme/windows-large-light.svg
    THEME_NAME := Hackneyed
    THEME_COMMENT := Windows 3.x-inspired cursors
endif
ifeq ($(VERBOSE), 1)
    ECHO=
else
    ECHO=@
endif

define HOTSPOT_GEN
	$(ECHO){\
		unset size_set; \
		for s in $(SIZES); do \
			size_set="$$size_set theme/$$s/$@"; \
		done; \
		cat $$size_set > $@ && echo ">>> created: $@"; \
	}
endef
define PNGGEN
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		src=$<; \
		for c in $(TWOHANDED_CURSORS) $(TWOHANDED_ANIMATED); do \
			if [ $$target = $$c ] || echo $$target|grep -E "$$c-[[:digit:]]+"; then \
				export src=$(TWOHANDED_SOURCE); \
				break; \
			fi; \
		done; \
		echo ">>> target: $@"; \
		./make-png.sh inkscape_bin=$(INKSCAPE) use_slicer=0 src=$$src target=$$target size=`echo $@|cut -d. -f2` smallest_size=$(SIZE_SMALL) output=$@; \
	}
endef
define WINDOWS_LARGE_PNGGEN
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		hotspots=`grep "\b$${name}\b" theme/windows-large.hotspots|cut -d: -f2|tr -d '\t'`; \
		echo ">>> target (Windows source): $@"; \
		./make-png.sh inkscape_bin=$(INKSCAPE) src=$< target=$$target size=`echo $@|cut -d. -f2` smallest_size=32 output=$@; \
	}
endef
define WINDOWS_CURSOR_GEN
	ACTUAL_HOTSPOTS=$(foreach hotspot,$(CUSTOM_HOTSPOTS),-s $(hotspot))
	$(ECHO){\
		set -- `cat $<`; \
		x=$$2; \
		y=$$3; \
		set -- $^; \
		shift 2; \
		./png2cur -s 32x32@$$x,$$y $(ACTUAL_HOTSPOTS) -o $@ $$*; \
	}
endef
define WINDOWS_LARGE_CURSORGEN
	$(ECHO){\
		target=`echo $@|cut -d. -f1|sed 's/_large//g'`; \
		set -- `grep "\b$${target}\b" theme/windows-large.hotspots |cut -d: -f2`; \
		for h in $$* $(CUSTOM_HOTSPOTS); do \
			echo $$h; \
			hotspots="$$hotspots -s $$h"; \
		done; \
		set -- $^; \
		shift 2; \
		./png2cur $$hotspots -o $@ $$*; \
	}
endef

THEME_NAME_SMALL := $(THEME_NAME)-$(SIZE_SMALL)px
THEME_NAME_MEDIUM := $(THEME_NAME)-$(SIZE_MEDIUM)px
THEME_NAME_LARGE := $(THEME_NAME)-$(SIZE_LARGE)px
THEME_EXAMPLE := default
THEME_WINDOWS := $(THEME_NAME)-Windows-$(VERSION)

SIZES ?= $(SIZE_SMALL) $(SIZE_MEDIUM) $(SIZE_LARGE) $(SIZE_LARGE1) $(SIZE_LARGE2) $(SIZE_LARGE3) $(SIZE_LARGE4)
XCURSORGEN := /usr/bin/xcursorgen

TWOHANDED_CURSORS := text \
	vertical-text \
	sb_up_arrow \
	sb_down_arrow \
	sb_left_arrow \
	sb_right_arrow \
	pirate \
	based_arrow_up \
	based_arrow_down \
	ns-resize \
	ew-resize \
	nwse-resize \
	nesw-resize \
	move \
	plus \
	crosshair \
	n-resize \
	s-resize \
	w-resize \
	e-resize \
	nw-resize \
	ne-resize \
	sw-resize \
	se-resize \
	center_ptr \
	all-scroll \
	not-allowed \
	draped_box \
	wayland-cursor \
	X_cursor \
	ul_angle \
	ur_angle \
	ll_angle \
	lr_angle \
	split_v \
	split_h \
	top_tee \
	bottom_tee \
	left_tee \
	right_tee

# I'm assuming right-handed as default for brevity's sake
# please don't send me death threats
CURSORS := alias \
	color-picker \
	context-menu \
	copy \
	default \
	help \
	pencil \
	dnd-move \
	zoom \
	zoom-in \
	zoom-out \
	no-drop \
	draft \
	right_ptr \
	openhand \
	closedhand \
	pointer \
	coffee_mug \
	exchange

WINCURSORS := default.cur \
	help.cur \
	text.cur \
	crosshair.cur \
	pencil.cur \
	ns-resize.cur \
	ew-resize.cur \
	nesw-resize.cur \
	nwse-resize.cur \
	sb_up_arrow.cur \
	pointer.cur \
	move.cur \
	not-allowed.cur

WINCURSORS_LARGE := $(WINCURSORS:.cur=_large.cur)

TWOHANDED_ANIMATED := wait
CURSORS_ANIMATED := progress
WINCURSORS_ANI := wait.ani progress.ani
WINCURSORS_LARGE_ANI := wait_large.ani progress_large.ani
LWINCURSORS := default_left.cur help_left.cur pencil_left.cur pointer_left.cur
LWINCURSORS_LARGE := default_left_large.cur help_left_large.cur pencil_left_large.cur pointer_left_large.cur
LWINCURSORS_ANI := progress_left.ani
LWINCURSORS_LARGE_ANI := progress_left_large.ani
WAIT_FRAMES := 16
WAIT_DEFAULT_FRAMETIME := 30
WAIT_CUSTOM_FRAMETIMES := frame_5_time=300
PROGRESS_FRAMES := 16
PROGRESS_DEFAULT_FRAMETIME := 30
PROGRESS_CUSTOM_FRAMETIMES := frame_5_time=300
WINDOWS_WAIT_DEFAULT_FRAMETIME := 2
WINDOWS_WAIT_CUSTOM_FRAMETIMES := frame_5_time=15
WINDOWS_PROGRESS_DEFAULT_FRAMETIME := 2
WINDOWS_PROGRESS_CUSTOM_FRAMETIMES := frame_5_time=15

TWOHANDED_SMALL := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_SMALL))
TWOHANDED_MEDIUM := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_MEDIUM))
TWOHANDED_LARGE := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_LARGE))
TWOHANDED_LARGE1 := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_LARGE1))
TWOHANDED_LARGE2 := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_LARGE2))
TWOHANDED_LARGE3 := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_LARGE3))
TWOHANDED_LARGE4 := $(foreach common,$(TWOHANDED_CURSORS),$(common).$(SIZE_LARGE4))

TWOHANDED_ANIMATED_SMALL := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_SMALL))
TWOHANDED_ANIMATED_MEDIUM := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_MEDIUM))
TWOHANDED_ANIMATED_LARGE := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_LARGE))
TWOHANDED_ANIMATED_LARGE1 := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_LARGE1))
TWOHANDED_ANIMATED_LARGE2 := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_LARGE2))
TWOHANDED_ANIMATED_LARGE3 := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_LARGE3))
TWOHANDED_ANIMATED_LARGE4 := $(foreach ani_common,$(TWOHANDED_ANIMATED),$(ani_common).$(SIZE_LARGE4))

PNG_TWOHANDED_SMALL := $(foreach png_common,$(TWOHANDED_SMALL),$(png_common).png)
PNG_TWOHANDED_MEDIUM := $(foreach png_common,$(TWOHANDED_MEDIUM),$(png_common).png)
PNG_TWOHANDED_LARGE := $(foreach png_common,$(TWOHANDED_LARGE),$(png_common).png)
PNG_TWOHANDED_LARGE1 := $(foreach png_common,$(TWOHANDED_LARGE1),$(png_common).png)
PNG_TWOHANDED_LARGE2 := $(foreach png_common,$(TWOHANDED_LARGE2),$(png_common).png)
PNG_TWOHANDED_LARGE3 := $(foreach png_common,$(TWOHANDED_LARGE3),$(png_common).png)
PNG_TWOHANDED_LARGE4 := $(foreach png_common,$(TWOHANDED_LARGE4),$(png_common).png)

CURSORS_SMALL := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_SMALL))
CURSORS_MEDIUM := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_MEDIUM))
CURSORS_LARGE := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_LARGE))
CURSORS_LARGE1 := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_LARGE1))
CURSORS_LARGE2 := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_LARGE2))
CURSORS_LARGE3 := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_LARGE3))
CURSORS_LARGE4 := $(foreach righthanded_cursors,$(CURSORS),$(righthanded_cursors).$(SIZE_LARGE4))

CURSORS_ANIMATED_SMALL := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_SMALL))
CURSORS_ANIMATED_MEDIUM := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_MEDIUM))
CURSORS_ANIMATED_LARGE := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_LARGE))
CURSORS_ANIMATED_LARGE1 := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_LARGE1))
CURSORS_ANIMATED_LARGE2 := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_LARGE2))
CURSORS_ANIMATED_LARGE3 := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_LARGE3))
CURSORS_ANIMATED_LARGE4 := $(foreach righthanded_ani,$(CURSORS_ANIMATED),$(righthanded_ani).$(SIZE_LARGE4))

PNG_SMALL := $(foreach png_handed,$(CURSORS_SMALL),$(png_handed).png)
PNG_MEDIUM :=$(foreach png_handed,$(CURSORS_MEDIUM),$(png_handed).png)
PNG_LARGE := $(foreach png_handed,$(CURSORS_LARGE),$(png_handed).png)
PNG_LARGE1 := $(foreach png_handed,$(CURSORS_LARGE1),$(png_handed).png)
PNG_LARGE2 := $(foreach png_handed,$(CURSORS_LARGE2),$(png_handed).png)
PNG_LARGE3 := $(foreach png_handed,$(CURSORS_LARGE3),$(png_handed).png)
PNG_LARGE4 := $(foreach png_handed,$(CURSORS_LARGE4),$(png_handed).png)

LEFTHANDED := $(foreach lefthanded,$(CURSORS),$(lefthanded).left)
LEFTHANDED_SMALL := $(LEFTHANDED:.left=.$(SIZE_SMALL).left)
LEFTHANDED_MEDIUM := $(LEFTHANDED:.left=.$(SIZE_MEDIUM).left)
LEFTHANDED_LARGE := $(LEFTHANDED:.left=.$(SIZE_LARGE).left)
LEFTHANDED_LARGE1 := $(LEFTHANDED:.left=.$(SIZE_LARGE1).left)
LEFTHANDED_LARGE2 := $(LEFTHANDED:.left=.$(SIZE_LARGE2).left)
LEFTHANDED_LARGE3 := $(LEFTHANDED:.left=.$(SIZE_LARGE3).left)
LEFTHANDED_LARGE4 := $(LEFTHANDED:.left=.$(SIZE_LARGE4).left)

LEFTHANDED_ANIMATED := $(foreach left_ani,$(CURSORS_ANIMATED),$(left_ani).left)
LEFTHANDED_ANIMATED_SMALL := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_SMALL).left)
LEFTHANDED_ANIMATED_MEDIUM := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_MEDIUM).left)
LEFTHANDED_ANIMATED_LARGE := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_LARGE).left)
LEFTHANDED_ANIMATED_LARGE1 := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_LARGE1).left)
LEFTHANDED_ANIMATED_LARGE2 := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_LARGE2).left)
LEFTHANDED_ANIMATED_LARGE3 := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_LARGE3).left)
LEFTHANDED_ANIMATED_LARGE4 := $(LEFTHANDED_ANIMATED:.left=.$(SIZE_LARGE4).left)

LPNG_SMALL := $(LEFTHANDED_SMALL:.$(SIZE_SMALL).left=.$(SIZE_SMALL).left.png)
LPNG_SMALL := $(foreach lpng,$(LEFTHANDED_SMALL),$(lpng).png)
LPNG_MEDIUM := $(foreach lpng,$(LEFTHANDED_MEDIUM),$(lpng).png)
LPNG_LARGE := $(foreach lpng,$(LEFTHANDED_LARGE),$(lpng).png)
LPNG_LARGE1 := $(foreach lpng,$(LEFTHANDED_LARGE1),$(lpng).png)
LPNG_LARGE2 := $(foreach lpng,$(LEFTHANDED_LARGE2),$(lpng).png)
LPNG_LARGE3 := $(foreach lpng,$(LEFTHANDED_LARGE3),$(lpng).png)
LPNG_LARGE4 := $(foreach lpng,$(LEFTHANDED_LARGE4),$(lpng).png)

all-sizes: all.small all.medium all.large all.small.left all.medium.left all.large.left all.left all
all-themes: theme theme-svg theme.left theme-svg.left theme.small theme.medium theme.large theme.small.left theme.medium.left theme.large.left
all-dist: dist.left dist.small dist.medium dist.large dist.small.left dist.medium.left dist.large.left dist
install-all: install install.small install.medium install.large install.left install.small.left install.medium.left install.large.left

install: theme theme-svg
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a $(THEME_NAME) -t $(DESTDIR)$(PREFIX)/share/icons

install.small: theme.small
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a $(THEME_NAME_SMALL) -t $(DESTDIR)$(PREFIX)/share/icons

install.medium: theme.medium
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a $(THEME_NAME_MEDIUM) -t $(DESTDIR)$(PREFIX)/share/icons

install.large: theme.large
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a $(THEME_NAME_LARGE) -t $(DESTDIR)$(PREFIX)/share/icons

install.left: theme.left theme-svg.left
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a L$(THEME_NAME) -t $(DESTDIR)$(PREFIX)/share/icons

install.small.left: theme.small.left
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a L$(THEME_NAME_SMALL) -t $(DESTDIR)$(PREFIX)/share/icons

install.medium.left: theme.medium.left
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a L$(THEME_NAME_MEDIUM) -t $(DESTDIR)$(PREFIX)/share/icons

install.large.left: theme.large.left
	test -e $(DESTDIR)$(PREFIX)/share/icons || $(INSTALL) -d -m755 $(DESTDIR)$(PREFIX)/share/icons
	cp -a L$(THEME_NAME_LARGE) -t $(DESTDIR)$(PREFIX)/share/icons

source-dist:
	git archive --format=tar.gz --prefix=hackneyed-x11-cursors-$(VERSION)/ HEAD > \
		hackneyed-x11-cursors-$(VERSION).tar.gz

windows-cursors: $(WINCURSORS) $(LWINCURSORS) $(WINCURSORS_ANI) $(LWINCURSORS_ANI) $(WINCURSORS_LARGE) $(LWINCURSORS_LARGE) $(WINCURSORS_LARGE_ANI) $(LWINCURSORS_LARGE_ANI)
	rm -rf $(THEME_WINDOWS) $(THEME_WINDOWS).zip
	mkdir -p $(THEME_WINDOWS)/Standard $(THEME_WINDOWS)/King-size
	cp $(WINCURSORS) $(LWINCURSORS) $(WINCURSORS_ANI) $(LWINCURSORS_ANI) $(THEME_WINDOWS)/Standard
	cp $(WINCURSORS_LARGE) $(LWINCURSORS_LARGE) $(WINCURSORS_LARGE_ANI) $(LWINCURSORS_LARGE_ANI) $(THEME_WINDOWS)/King-size
	zip -r $(THEME_WINDOWS).zip $(THEME_WINDOWS)

make-png.sh: inkscape-version

dist: theme theme-svg
	tar -jcof $(THEME_NAME)-$(VERSION)-right-handed.tar.bz2 $(THEME_NAME)

dist.left: theme.left theme-svg.left
	tar -jcof $(THEME_NAME)-$(VERSION)-left-handed.tar.bz2 L$(THEME_NAME)

dist.small: theme.small
	tar -jcof $(THEME_NAME_SMALL)-$(VERSION)-right-handed.tar.bz2 $(THEME_NAME_SMALL)

dist.medium: theme.medium
	tar -jcof $(THEME_NAME_MEDIUM)-$(VERSION)-right-handed.tar.bz2 $(THEME_NAME_MEDIUM)

dist.large: theme.large
	tar -jcof $(THEME_NAME_LARGE)-$(VERSION)-right-handed.tar.bz2 $(THEME_NAME_LARGE)

dist.small.left: theme.small.left
	tar -jcof $(THEME_NAME_SMALL)-$(VERSION)-left-handed.tar.bz2 L$(THEME_NAME_SMALL)

dist.medium.left: theme.medium.left
	tar -jcof $(THEME_NAME_MEDIUM)-$(VERSION)-left-handed.tar.bz2 L$(THEME_NAME_MEDIUM)

dist.large.left: theme.large.left
	tar -jcof $(THEME_NAME_LARGE)-$(VERSION)-left-handed.tar.bz2 L$(THEME_NAME_LARGE)

# hideous code but anyway
theme-svg: theme
	mkdir -p $(THEME_NAME)/cursors_scalable
	for c in $(CURSORS); do \
		mkdir $(THEME_NAME)/cursors_scalable/$$c; \
		cp $(RSVG_SOURCE)/$${c}.svg $(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		$(JQ) -n --arg source_svg "$${c}.svg" \
			--argjson hotspot_x "$$1" \
			--argjson hotspot_y "$$2" \
			'[{filename: $$source_svg, hotspot_x: $$hotspot_x, hotspot_y: $$hotspot_y, nominal_size: 24}]' > $(THEME_NAME)/cursors_scalable/$${c}/metadata.json; \
	done
	for c in $(TWOHANDED_CURSORS); do \
		mkdir $(THEME_NAME)/cursors_scalable/$$c; \
		cp $(TWOHANDED_SOURCE)/$${c}.svg $(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		$(JQ) -n --arg source_svg "$${c}.svg" \
			--argjson hotspot_x "$$1" \
			--argjson hotspot_y "$$2" \
			'[{filename: $$source_svg, hotspot_x: $$hotspot_x, hotspot_y: $$hotspot_y, nominal_size: $(SIZE_SMALL)}]' > $(THEME_NAME)/cursors_scalable/$${c}/metadata.json||exit 1; \
	done
	for c in $(TWOHANDED_ANIMATED); do \
		mkdir $(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		for i in `seq 1 $(WAIT_FRAMES)`; do \
			cp $(TWOHANDED_SOURCE)/$${c}-$${i}.svg $(THEME_NAME)/cursors_scalable/$${c} || exit 1; \
			$(WAIT_CUSTOM_FRAMETIMES); \
			eval frametime=\$$frame_$${i}_time; \
			test -z "$$frametime" && frametime=$(WAIT_DEFAULT_FRAMETIME); \
			if test -z "$$JSON_ARGS"; then \
				JSON_ARGS="{\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			else \
				JSON_ARGS="$${JSON_ARGS}, {\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			fi; \
		done; \
		echo "[$$JSON_ARGS]"|$(JQ) . > $(THEME_NAME)/cursors_scalable/$$c/metadata.json || exit 1; \
	done
	for c in $(CURSORS_ANIMATED); do \
		mkdir $(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		for i in `seq 1 $(PROGRESS_FRAMES)`; do \
			cp $(RSVG_SOURCE)/$${c}-$${i}.svg $(THEME_NAME)/cursors_scalable/$${c}/||exit 1; \
			$(PROGRESS_CUSTOM_FRAMETIMES); \
			eval frametime=\$$frame_$${i}_time; \
			test -z "$$frametime" && frametime=$(PROGRESS_DEFAULT_FRAMETIME); \
			if test -z "$$JSON_ARGS"; then \
				JSON_ARGS="{\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			else \
				JSON_ARGS="$${JSON_ARGS}, {\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			fi; \
		done; \
		echo "[$$JSON_ARGS]"|$(JQ) . > $(THEME_NAME)/cursors_scalable/$$c/metadata.json || exit 1; \
	done
	./do-symlinks.sh $(THEME_NAME)/cursors_scalable

theme-svg.left: theme.left
	mkdir -p L$(THEME_NAME)/cursors_scalable
	for c in $(CURSORS); do \
		mkdir L$(THEME_NAME)/cursors_scalable/$$c; \
		cp $(LSVG_SOURCE)/$${c}.svg L$(THEME_NAME)/cursors_scalable/$$c||exit 1; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}_left.in`; \
		shift; \
		$(JQ) -n --arg source_svg "$${c}.svg" \
			--argjson hotspot_x "$$1" \
			--argjson hotspot_y "$$2" \
			'[{filename: $$source_svg, hotspot_x: $$hotspot_x, hotspot_y: $$hotspot_y, nominal_size: 24}]' > L$(THEME_NAME)/cursors_scalable/$${c}/metadata.json; \
	done
	for c in $(TWOHANDED_CURSORS); do \
		mkdir L$(THEME_NAME)/cursors_scalable/$$c; \
		cp $(TWOHANDED_SOURCE)/$${c}.svg L$(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		$(JQ) -n --arg source_svg "$${c}.svg" \
			--argjson hotspot_x "$$1" \
			--argjson hotspot_y "$$2" \
			'[{filename: $$source_svg, hotspot_x: $$hotspot_x, hotspot_y: $$hotspot_y, nominal_size: $(SIZE_SMALL)}]' > L$(THEME_NAME)/cursors_scalable/$${c}/metadata.json||exit 1; \
	done
	for c in $(TWOHANDED_ANIMATED); do \
		mkdir L$(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}.in`; \
		shift; \
		for i in `seq 1 $(WAIT_FRAMES)`; do \
			cp $(TWOHANDED_SOURCE)/$${c}-$${i}.svg L$(THEME_NAME)/cursors_scalable/$${c}/$${c}-$${i}.svg || exit 1; \
			$(WAIT_CUSTOM_FRAMETIMES); \
			eval frametime=\$$frame_$${i}_time; \
			test -z "$$frametime" && frametime=30; \
			if test -z "$$JSON_ARGS"; then \
				JSON_ARGS="{\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			else \
				JSON_ARGS="$${JSON_ARGS}, {\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			fi; \
		done; \
		echo "[$$JSON_ARGS]"|$(JQ) . > L$(THEME_NAME)/cursors_scalable/$$c/metadata.json || exit 1; \
	done
	for c in $(CURSORS_ANIMATED); do \
		mkdir L$(THEME_NAME)/cursors_scalable/$$c; \
		set -- `cat theme/$(SIZE_SMALL)/$${c}_left.in`; \
		shift; \
		for i in `seq 1 $(PROGRESS_FRAMES)`; do \
			cp $(LSVG_SOURCE)/$${c}-$${i}.svg L$(THEME_NAME)/cursors_scalable/$${c}/||exit 1; \
			$(PROGRESS_CUSTOM_FRAMETIMES); \
			eval frametime=\$$frame_$${i}_time; \
			test -z "$$frametime" && frametime=$(PROGRESS_DEFAULT_FRAMETIME); \
			if test -z "$$JSON_ARGS"; then \
				JSON_ARGS="{\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			else \
				JSON_ARGS="$${JSON_ARGS}, {\"filename\": \"$${c}-$${i}.svg\", \"delay\": $$frametime, \"hotspot_x\": $$1, \"hotspot_y\": $$2, \"nominal_size\": $(SIZE_SMALL)}"; \
			fi; \
		done; \
		echo "[$$JSON_ARGS]"|$(JQ) . > L$(THEME_NAME)/cursors_scalable/$$c/metadata.json || exit 1; \
	done
	./do-symlinks.sh L$(THEME_NAME)/cursors_scalable

theme: all
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/scalable/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > $(THEME_NAME)/index.theme
	./do-symlinks.sh $(THEME_NAME)/cursors

theme.left: all.left
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/left-handed, scalable/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > L$(THEME_NAME)/index.theme
	./do-symlinks.sh L$(THEME_NAME)/cursors

theme.small: all.small
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_SMALL)x$(SIZE_SMALL)/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > $(THEME_NAME_SMALL)/index.theme
	./do-symlinks.sh $(THEME_NAME_SMALL)/cursors

theme.medium: all.medium
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_MEDIUM)x$(SIZE_MEDIUM)/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > $(THEME_NAME_MEDIUM)/index.theme
	./do-symlinks.sh $(THEME_NAME_MEDIUM)/cursors

theme.large: all.large
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_LARGE)x$(SIZE_LARGE)/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > $(THEME_NAME_LARGE)/index.theme
	./do-symlinks.sh $(THEME_NAME_LARGE)/cursors

theme.small.left: all.small.left
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_SMALL)x$(SIZE_SMALL), left-handed/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > L$(THEME_NAME_SMALL)/index.theme
	./do-symlinks.sh L$(THEME_NAME_SMALL)/cursors

theme.medium.left: all.medium.left
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_MEDIUM)x$(SIZE_MEDIUM), left-handed/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > L$(THEME_NAME_MEDIUM)/index.theme
	./do-symlinks.sh L$(THEME_NAME_MEDIUM)/cursors

theme.large.left: all.large.left
	sed "s/THEME_NAME/$(THEME_NAME)/1; \
		s/THEME_DESC/$(SIZE_LARGE)x$(SIZE_LARGE), left-handed/1; \
		s/THEME_COMMENT/$(THEME_COMMENT)/1; \
		s/THEME_EXAMPLE/$(THEME_EXAMPLE)/1" index.theme.template > L$(THEME_NAME_LARGE)/index.theme
	./do-symlinks.sh L$(THEME_NAME_LARGE)/cursors

all: $(CURSORS) $(CURSORS_ANIMATED) $(TWOHANDED_CURSORS) $(TWOHANDED_ANIMATED)
	rm -rf $(THEME_NAME)
	mkdir -p $(THEME_NAME)/cursors
	cp $(CURSORS) $(CURSORS_ANIMATED) $(TWOHANDED_CURSORS) $(TWOHANDED_ANIMATED) $(THEME_NAME)/cursors

all.left: $(LEFTHANDED) $(LEFTHANDED_ANIMATED) $(TWOHANDED_CURSORS) $(TWOHANDED_ANIMATED)
	rm -rf L$(THEME_NAME)
	mkdir -p L$(THEME_NAME)/cursors
	cp $(TWOHANDED_CURSORS) $(TWOHANDED_ANIMATED) L$(THEME_NAME)/cursors
	for l in $(LEFTHANDED) $(LEFTHANDED_ANIMATED); do \
		new_name=`echo $$l|sed 's/.left$$//'`; \
		cp $$l L$(THEME_NAME)/cursors/$$new_name; \
	done

all.small: $(CURSORS_SMALL) $(CURSORS_ANIMATED_SMALL) $(TWOHANDED_SMALL) $(TWOHANDED_ANIMATED_SMALL)
	rm -rf $(THEME_NAME_SMALL)
	mkdir -p $(THEME_NAME_SMALL)/cursors
	for l in $(CURSORS_SMALL) $(TWOHANDED_SMALL) $(CURSORS_ANIMATED_SMALL) $(TWOHANDED_ANIMATED_SMALL); do \
		new_name=`echo $$l|sed 's/.$(SIZE_SMALL)$$//'`; \
		cp $$l $(THEME_NAME_SMALL)/cursors/$$new_name; \
	done

all.medium: $(CURSORS_MEDIUM) $(TWOHANDED_MEDIUM) $(CURSORS_ANIMATED_MEDIUM) $(TWOHANDED_ANIMATED_MEDIUM)
	rm -rf $(THEME_NAME_MEDIUM)
	mkdir -p $(THEME_NAME_MEDIUM)/cursors
	for l in $(CURSORS_MEDIUM) $(TWOHANDED_MEDIUM) $(CURSORS_ANIMATED_MEDIUM) $(TWOHANDED_ANIMATED_MEDIUM); do \
		new_name=`echo $$l|sed 's/.$(SIZE_MEDIUM)$$//'`; \
		cp $$l $(THEME_NAME_MEDIUM)/cursors/$$new_name; \
	done

all.large: $(CURSORS_LARGE) $(TWOHANDED_LARGE) $(CURSORS_ANIMATED_LARGE) $(TWOHANDED_ANIMATED_LARGE)
	rm -rf $(THEME_NAME_LARGE)
	mkdir -p $(THEME_NAME_LARGE)/cursors
	for l in $(CURSORS_LARGE) $(TWOHANDED_LARGE) $(TWOHANDED_ANIMATED_LARGE) $(CURSORS_ANIMATED_LARGE); do \
		new_name=`echo $$l|sed 's/.$(SIZE_LARGE)$$//'`; \
		cp $$l $(THEME_NAME_LARGE)/cursors/$$new_name; \
	done

all.small.left: $(LEFTHANDED_SMALL) $(TWOHANDED_SMALL) $(LEFTHANDED_ANIMATED_SMALL) $(TWOHANDED_ANIMATED_SMALL)
	rm -rf L$(THEME_NAME_SMALL)
	mkdir -p L$(THEME_NAME_SMALL)/cursors
	for l in $(TWOHANDED_SMALL) $(TWOHANDED_ANIMATED_SMALL); do \
		new_name=`echo $$l|sed 's/.$(SIZE_SMALL)$$//'`; \
		cp $$l L$(THEME_NAME_SMALL)/cursors/$$new_name; \
	done
	for l in $(LEFTHANDED_SMALL) $(LEFTHANDED_ANIMATED_SMALL); do \
		new_name=`echo $$l|sed 's/.$(SIZE_SMALL).left$$//'`; \
		cp $$l L$(THEME_NAME_SMALL)/cursors/$$new_name; \
	done

all.medium.left: $(LEFTHANDED_MEDIUM) $(TWOHANDED_MEDIUM) $(LEFTHANDED_ANIMATED_MEDIUM) $(TWOHANDED_ANIMATED_MEDIUM)
	rm -rf L$(THEME_NAME_MEDIUM)
	mkdir -p L$(THEME_NAME_MEDIUM)/cursors
	for l in $(TWOHANDED_MEDIUM) $(TWOHANDED_ANIMATED_MEDIUM); do \
		new_name=`echo $$l|sed 's/.$(SIZE_MEDIUM)$$//'`; \
		cp $$l L$(THEME_NAME_MEDIUM)/cursors/$$new_name; \
	done
	for l in $(LEFTHANDED_MEDIUM) $(LEFTHANDED_ANIMATED_MEDIUM); do \
		new_name=`echo $$l|sed 's/.$(SIZE_MEDIUM).left$$//'`; \
		cp $$l L$(THEME_NAME_MEDIUM)/cursors/$$new_name; \
	done

all.large.left: $(LEFTHANDED_LARGE) $(TWOHANDED_LARGE) $(LEFTHANDED_ANIMATED_LARGE) $(TWOHANDED_ANIMATED_LARGE)
	rm -rf L$(THEME_NAME_LARGE)
	mkdir -p L$(THEME_NAME_LARGE)/cursors
	for l in $(TWOHANDED_LARGE) $(TWOHANDED_ANIMATED_LARGE); do \
		new_name=`echo $$l|sed 's/.$(SIZE_LARGE)$$//'`; \
		cp $$l L$(THEME_NAME_LARGE)/cursors/$$new_name; \
	done
	for l in $(LEFTHANDED_LARGE) $(LEFTHANDED_ANIMATED_LARGE); do \
		new_name=`echo $$l|sed 's/.$(SIZE_LARGE).left$$//'`; \
		cp $$l L$(THEME_NAME_LARGE)/cursors/$$new_name; \
	done

%.in: theme/*/%.in
	$(HOTSPOT_GEN)

%_left.in: theme/*/%_left.in
	$(HOTSPOT_GEN)

inkscape-version: $(INKSCAPE)
	$(ECHO)env DBUS_SESSION_BUS_ADDRESS="" $(INKSCAPE) --version | cut -d' ' -f2 > inkscape-version

%.png: $(RSVG_SOURCE) $(TWOHANDED_SOURCE) make-png.sh
	$(PNGGEN)

%.left.png: $(LSVG_SOURCE) $(TWOHANDED_SOURCE) make-png.sh
	$(PNGGEN)

%: %.in %.$(SIZE_SMALL).png %.$(SIZE_MEDIUM).png %.$(SIZE_LARGE).png\
	%.$(SIZE_LARGE1).png %.$(SIZE_LARGE2).png %.$(SIZE_LARGE3).png %.$(SIZE_LARGE4).png
	$(XCURSORGEN) $< $@

%.left: %_left.in %.$(SIZE_SMALL).left.png %.$(SIZE_MEDIUM).left.png %.$(SIZE_LARGE).left.png\
	%.$(SIZE_LARGE1).left.png %.$(SIZE_LARGE2).left.png %.$(SIZE_LARGE3).left.png %.$(SIZE_LARGE4).left.png
	$(XCURSORGEN) $< $@

%.$(SIZE_SMALL): theme/$(SIZE_SMALL)/%.in %.$(SIZE_SMALL).png
	$(XCURSORGEN) $< $@

%.$(SIZE_MEDIUM): theme/$(SIZE_MEDIUM)/%.in %.$(SIZE_MEDIUM).png
	$(XCURSORGEN) $< $@

%.$(SIZE_LARGE): theme/$(SIZE_LARGE)/%.in %.$(SIZE_LARGE).png
	$(XCURSORGEN) $< $@

%.$(SIZE_SMALL).left: theme/$(SIZE_SMALL)/%_left.in %.$(SIZE_SMALL).left.png
	$(XCURSORGEN) $< $@

%.$(SIZE_MEDIUM).left: theme/$(SIZE_MEDIUM)/%_left.in %.$(SIZE_MEDIUM).left.png
	$(XCURSORGEN) $< $@

%.$(SIZE_LARGE).left: theme/$(SIZE_LARGE)/%_left.in %.$(SIZE_LARGE).left.png
	$(XCURSORGEN) $< $@

png2cur: png2cur.c
	$(CC) -std=c99 -Wall -Werror -pedantic -g -o png2cur png2cur.c \
		-lm `pkg-config --cflags --libs libpng MagickWand` \
		-DWAND_VERSION="`pkg-config --modversion MagickWand|tr -d .|cut -b1-3`"

animaker: animaker.c
	$(CC) -std=c99 -Wall -Werror -pedantic -g -o animaker animaker.c

%_left.cur: theme/$(SIZE_SMALL)/%_left.in png2cur %.$(SIZE_SMALL).left.png %.48.left.png %.64.left.png %.96.left.png %.128.left.png
	$(WINDOWS_CURSOR_GEN)

%.cur: theme/$(SIZE_SMALL)/%.in png2cur %.$(SIZE_SMALL).png %.48.png %.64.png %.96.png %.128.png
	$(WINDOWS_CURSOR_GEN)

%_large.32.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_large.48.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_large.64.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_large.96.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_large.128.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_left_large.32.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_left_large.48.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_left_large.64.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_left_large.96.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_left_large.128.png: $(WINDOWS_SOURCE) make-png.sh
	$(WINDOWS_LARGE_PNGGEN)

%_large.cur: theme/windows-large.hotspots png2cur %_large.32.png %_large.48.png %_large.64.png %_large.96.png %_large.128.png
	$(WINDOWS_LARGE_CURSORGEN)

%_left_large.cur: theme/windows-large.hotspots png2cur %_left_large.32.png %_left_large.48.png %_left_large.64.png %_left_large.96.png %_left_large.128.png
	$(WINDOWS_LARGE_CURSORGEN)

wait_all_frames: wait.$(SIZE_SMALL).frames wait.$(SIZE_MEDIUM).frames \
	wait.$(SIZE_LARGE).frames wait.$(SIZE_LARGE1).frames wait.$(SIZE_LARGE2).frames \
	wait.$(SIZE_LARGE3).frames wait.$(SIZE_LARGE4).frames

wait_all_hotspots: wait.$(SIZE_SMALL).in wait.$(SIZE_MEDIUM).in wait.$(SIZE_LARGE).in \
	wait.$(SIZE_LARGE1).in wait.$(SIZE_LARGE2).in wait.$(SIZE_LARGE3).in wait.$(SIZE_LARGE4).in

progress_all_frames: progress.$(SIZE_SMALL).frames progress.$(SIZE_MEDIUM).frames \
	progress.$(SIZE_LARGE).frames progress.$(SIZE_LARGE1).frames progress.$(SIZE_LARGE2).frames \
	progress.$(SIZE_LARGE3).frames progress.$(SIZE_LARGE4).frames

progress_all_hotspots: progress.$(SIZE_SMALL).in progress.$(SIZE_MEDIUM).in progress.$(SIZE_LARGE).in \
	progress.$(SIZE_LARGE1).in progress.$(SIZE_LARGE2).in progress.$(SIZE_LARGE3).in progress.$(SIZE_LARGE4).in

progress_left_all_frames: progress.left.$(SIZE_SMALL).frames progress.left.$(SIZE_MEDIUM).frames \
	progress.left.$(SIZE_LARGE).frames progress.left.$(SIZE_LARGE1).frames progress.left.$(SIZE_LARGE2).frames \
	progress.left.$(SIZE_LARGE3).frames progress.left.$(SIZE_LARGE4).frames

progress_left_all_hotspots: progress.left.$(SIZE_SMALL).in progress.left.$(SIZE_MEDIUM).in progress.left.$(SIZE_LARGE).in \
	progress.left.$(SIZE_LARGE1).in progress.left.$(SIZE_LARGE2).in \
	progress.left.$(SIZE_LARGE3).in progress.left.$(SIZE_LARGE4).in

wait.%.frames: $(TWOHANDED_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-png.sh src=$< use_slicer=0 target=$$target size=$$size smallest_size=$(SIZE_SMALL) frames=$(WAIT_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

progress.%.frames: $(RSVG_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-png.sh src=$< use_slicer=0 target=$$target size=$$size smallest_size=$(SIZE_SMALL) frames=$(PROGRESS_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

progress.left.%.frames: $(LSVG_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1,2`; \
		size=`echo $@|cut -d. -f3`; \
		./make-png.sh src=$< use_slicer=0 target=$$target size=$$size smallest_size=$(SIZE_SMALL) frames=$(PROGRESS_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

wait.%.in: theme/%/wait.in make-ani-hotspots.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-ani-hotspots.sh target=$$target size=$$size frames=$(WAIT_FRAMES) \
			default_frametime=$(WAIT_DEFAULT_FRAMETIME) \
			$(WAIT_CUSTOM_FRAMETIMES); \
	}

wait.%: wait.%.in wait.%.frames
	$(XCURSORGEN) $< $@

wait: wait_all_frames wait_all_hotspots
	cat wait.*.in|$(XCURSORGEN) - $@

# use SIZE_SMALL instead of a hardcoded size, otherwise it will confuse the hell out of
# make-png.sh when using a different base size
wait.ani: png2cur animaker make-windows-ani.sh wait.$(SIZE_SMALL).frames wait.48.frames wait.64.frames
	./make-windows-ani.sh target=wait output_ani=$@ frames=$(WAIT_FRAMES) \
		default_frametime=$(WINDOWS_WAIT_DEFAULT_FRAMETIME) $(WINDOWS_WAIT_CUSTOM_FRAMETIMES) \
		hotspots='48x48@23,23 64x64@31,31'

wait_large.%.frames: $(WINDOWS_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-png.sh src=$< target=wait_large size=$$size smallest_size=32 frames=$(WAIT_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

progress_large.%.frames: $(WINDOWS_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-png.sh src=$< target=progress_large size=$$size smallest_size=32 frames=$(PROGRESS_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

progress_left_large.%.frames: $(WINDOWS_SOURCE) make-png.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-png.sh src=$< target=progress_left_large size=$$size smallest_size=32 frames=$(PROGRESS_FRAMES) inkscape_bin=$(INKSCAPE); \
	}

wait_large.ani: theme/windows-large.hotspots png2cur animaker make-windows-ani.sh wait_large.32.frames wait_large.48.frames wait_large.64.frames
	$(ECHO){\
		hotspots=`grep '\bwait\b' theme/windows-large.hotspots|cut -d: -f2|tr -d '\t'`; \
		./make-windows-ani.sh sizes='32 48 64' target=wait_large output_ani=$@ frames=$(WAIT_FRAMES) \
			default_frametime=$(WINDOWS_WAIT_DEFAULT_FRAMETIME) $(WINDOWS_WAIT_CUSTOM_FRAMETIMES) \
			hotspots="$$hotspots"; \
	}

progress_large.ani: theme/windows-large.hotspots png2cur animaker make-windows-ani.sh progress_large.32.frames progress_large.48.frames progress_large.64.frames
	$(ECHO){\
		hotspots=`grep '\bprogress\b' theme/windows-large.hotspots|cut -d: -f2|tr -d '\t'`; \
		./make-windows-ani.sh sizes='32 48 64' target=progress_large output_ani=$@ frames=$(PROGRESS_FRAMES) \
			default_frametime=$(WINDOWS_PROGRESS_DEFAULT_FRAMETIME) $(WINDOWS_PROGRESS_CUSTOM_FRAMETIMES) \
			hotspots="$$hotspots"; \
	}

progress_left_large.ani: png2cur animaker make-windows-ani.sh progress_left_large.32.frames progress_left_large.48.frames progress_left_large.64.frames
	$(ECHO){\
		hotspots=`grep '\bprogress_left\b' theme/windows-large.hotspots|cut -d: -f2|tr -d '\t'`; \
		./make-windows-ani.sh sizes='32 48 64' target=progress_left_large output_ani=$@ frames=$(PROGRESS_FRAMES) \
			default_frametime=$(WINDOWS_PROGRESS_DEFAULT_FRAMETIME) $(WINDOWS_PROGRESS_CUSTOM_FRAMETIMES) \
			hotspots="$$hotspots"; \
	}

progress.ani: png2cur animaker make-windows-ani.sh progress.$(SIZE_SMALL).frames progress.48.frames progress.64.frames
	./make-windows-ani.sh target=progress output_ani=$@ frames=$(PROGRESS_FRAMES) \
		default_frametime=$(WINDOWS_PROGRESS_DEFAULT_FRAMETIME) $(WINDOWS_PROGRESS_CUSTOM_FRAMETIMES)

progress_left.ani: png2cur animaker make-windows-ani.sh progress.left.$(SIZE_SMALL).frames progress.left.48.frames progress.left.64.frames
	./make-windows-ani.sh target=progress.left output_ani=$@ frames=$(PROGRESS_FRAMES) \
		default_frametime=$(WINDOWS_PROGRESS_DEFAULT_FRAMETIME) $(WINDOWS_PROGRESS_CUSTOM_FRAMETIMES)

progress: progress_all_frames progress_all_hotspots
	cat progress.*.in|$(XCURSORGEN) - $@

progress.%: progress.%.in progress.%.frames
	$(XCURSORGEN) $< $@

progress.%.in: theme/%/progress.in make-ani-hotspots.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		size=`echo $@|cut -d. -f2`; \
		./make-ani-hotspots.sh target=$$target size=$$size frames=$(PROGRESS_FRAMES) \
			default_frametime=$(PROGRESS_DEFAULT_FRAMETIME) \
			$(PROGRESS_CUSTOM_FRAMETIMES); \
	}

progress_left.%.in: theme/%/progress_left.in make-ani-hotspots.sh
	$(ECHO){\
		target=`echo $@|cut -d. -f1`; \
		target=`echo $$target|sed 's/_/./'`; \
		size=`echo $@|cut -d. -f2`; \
		./make-ani-hotspots.sh target=$$target size=$$size frames=$(PROGRESS_FRAMES) \
			default_frametime=$(PROGRESS_DEFAULT_FRAMETIME) \
			$(PROGRESS_CUSTOM_FRAMETIMES); \
	}

progress.left: progress_left_all_frames progress_left.$(SIZE_SMALL).in progress_left.$(SIZE_MEDIUM).in progress_left.$(SIZE_LARGE).in progress_left.$(SIZE_LARGE1).in progress_left.$(SIZE_LARGE2).in
	cat progress_left.*.in|$(XCURSORGEN) - $@

progress.%.left: progress_left.%.in progress.left.%.frames
	$(XCURSORGEN) $< $@

preview: $(PNG_SMALL) $(LPNG_SMALL) $(PNG_TWOHANDED_SMALL) wait-1.$(SIZE_SMALL).png progress-1.$(SIZE_SMALL).png progress-1.$(SIZE_SMALL).left.png
	montage -background none -mode concatenate -tile 9x6 -geometry +10+5 \
		default.$(SIZE_SMALL).png help.$(SIZE_SMALL).png progress-1.$(SIZE_SMALL).png \
		alias.$(SIZE_SMALL).png copy.$(SIZE_SMALL).png context-menu.$(SIZE_SMALL).png \
		no-drop.$(SIZE_SMALL).png dnd-move.$(SIZE_SMALL).png center_ptr.$(SIZE_SMALL).png \
		help.$(SIZE_SMALL).left.png progress-1.$(SIZE_SMALL).left.png alias.$(SIZE_SMALL).left.png \
		copy.$(SIZE_SMALL).left.png context-menu.$(SIZE_SMALL).left.png no-drop.$(SIZE_SMALL).left.png \
		dnd-move.$(SIZE_SMALL).left.png default.$(SIZE_SMALL).left.png right_ptr.$(SIZE_SMALL).left.png \
		wait-1.$(SIZE_SMALL).png pointer.$(SIZE_SMALL).png openhand.$(SIZE_SMALL).png \
		closedhand.$(SIZE_SMALL).png sw-resize.$(SIZE_SMALL).png se-resize.$(SIZE_SMALL).png w-resize.$(SIZE_SMALL).png \
		e-resize.$(SIZE_SMALL).png n-resize.$(SIZE_SMALL).png s-resize.$(SIZE_SMALL).png \
		nw-resize.$(SIZE_SMALL).png ne-resize.$(SIZE_SMALL).png split_v.$(SIZE_SMALL).png zoom.$(SIZE_SMALL).png \
		zoom-in.$(SIZE_SMALL).png zoom-out.$(SIZE_SMALL).png nesw-resize.$(SIZE_SMALL).png \
		nwse-resize.$(SIZE_SMALL).png ew-resize.$(SIZE_SMALL).png ns-resize.$(SIZE_SMALL).png split_h.$(SIZE_SMALL).png \
		text.$(SIZE_SMALL).png vertical-text.$(SIZE_SMALL).png move.$(SIZE_SMALL).png crosshair.$(SIZE_SMALL).png plus.$(SIZE_SMALL).png \
		not-allowed.$(SIZE_SMALL).png pirate.$(SIZE_SMALL).png X_cursor.$(SIZE_SMALL).png \
		wayland-cursor.$(SIZE_SMALL).png draft.$(SIZE_SMALL).png pencil.$(SIZE_SMALL).png \
		color-picker.$(SIZE_SMALL).png sb_up_arrow.$(SIZE_SMALL).png sb_right_arrow.$(SIZE_SMALL).png sb_left_arrow.$(SIZE_SMALL).png \
		preview-$(THEME_NAME).png
	montage -background none -mode concatenate -tile 4x4 -geometry +5+5 \
		default.$(SIZE_SMALL).png help.$(SIZE_SMALL).png progress-1.$(SIZE_SMALL).png no-drop.$(SIZE_SMALL).png wait-1.$(SIZE_SMALL).png \
		pencil.$(SIZE_SMALL).png zoom-in.$(SIZE_SMALL).png context-menu.$(SIZE_SMALL).png \
		pointer.$(SIZE_SMALL).png openhand.$(SIZE_SMALL).png closedhand.$(SIZE_SMALL).png pirate.$(SIZE_SMALL).png \
		n-resize.$(SIZE_SMALL).png s-resize.$(SIZE_SMALL).png w-resize.$(SIZE_SMALL).png e-resize.$(SIZE_SMALL).png \
		preview-small-$(THEME_NAME).png


distclean: clean
	rm -rf $(THEME_WINDOWS)
	rm -rf $(THEME_NAME) L$(THEME_NAME) $(THEME_NAME_SMALL) $(THEME_NAME_MEDIUM) $(THEME_NAME_LARGE) \
		L$(THEME_NAME_SMALL) L$(THEME_NAME_MEDIUM) L$(THEME_NAME_LARGE)

clean:
	rm -f $(CURSORS)
	rm -f $(CURSORS_SMALL)
	rm -f $(CURSORS_MEDIUM)
	rm -f $(CURSORS_LARGE)
	rm -f $(CURSORS_LARGE1)
	rm -f $(CURSORS_LARGE2)
	rm -f $(CURSORS_LARGE3)
	rm -f $(CURSORS_LARGE4)
	rm -f $(LEFTHANDED)
	rm -f $(LEFTHANDED_SMALL)
	rm -f $(LEFTHANDED_MEDIUM)
	rm -f $(LEFTHANDED_LARGE)
	rm -f $(LEFTHANDED_LARGE1)
	rm -f $(LEFTHANDED_LARGE2)
	rm -f $(LEFTHANDED_LARGE3)
	rm -f $(LEFTHANDED_LARGE4)
	rm -f $(PNG_SMALL)
	rm -f $(PNG_MEDIUM)
	rm -f $(PNG_LARGE)
	rm -f $(PNG_LARGE1)
	rm -f $(PNG_LARGE2)
	rm -f $(PNG_LARGE3)
	rm -f $(PNG_LARGE4)
	rm -f $(LPNG_SMALL)
	rm -f $(LPNG_MEDIUM)
	rm -f $(LPNG_LARGE)
	rm -f $(LPNG_LARGE1)
	rm -f $(LPNG_LARGE2)
	rm -f $(LPNG_LARGE3)
	rm -f $(LPNG_LARGE4)
	rm -f $(TWOHANDED_CURSORS)
	rm -f $(TWOHANDED_SMALL)
	rm -f $(TWOHANDED_MEDIUM)
	rm -f $(TWOHANDED_LARGE)
	rm -f $(TWOHANDED_LARGE1)
	rm -f $(TWOHANDED_LARGE2)
	rm -f $(TWOHANDED_LARGE3)
	rm -f $(TWOHANDED_LARGE4)
	rm -f $(PNG_TWOHANDED_SMALL)
	rm -f $(PNG_TWOHANDED_MEDIUM)
	rm -f $(PNG_TWOHANDED_LARGE)
	rm -f $(PNG_TWOHANDED_LARGE1)
	rm -f $(PNG_TWOHANDED_LARGE2)
	rm -f $(PNG_TWOHANDED_LARGE3)
	rm -f $(PNG_TWOHANDED_LARGE4)
	rm -f $(TWOHANDED_ANIMATED) $(CURSORS_ANIMATED) $(LEFTHANDED_ANIMATED)
	rm -f $(TWOHANDED_ANIMATED_SMALL) $(CURSORS_ANIMATED_SMALL) $(LEFTHANDED_ANIMATED_SMALL)
	rm -f $(TWOHANDED_ANIMATED_MEDIUM) $(CURSORS_ANIMATED_MEDIUM) $(LEFTHANDED_ANIMATED_MEDIUM)
	rm -f $(TWOHANDED_ANIMATED_LARGE) $(CURSORS_ANIMATED_LARGE) $(LEFTHANDED_ANIMATED_LARGE)
	rm -f $(TWOHANDED_ANIMATED_LARGE1) $(CURSORS_ANIMATED_LARGE1) $(LEFTHANDED_ANIMATED_LARGE1)
	rm -f $(TWOHANDED_ANIMATED_LARGE2) $(CURSORS_ANIMATED_LARGE2) $(LEFTHANDED_ANIMATED_LARGE2)
	{\
		for c in $(TWOHANDED_ANIMATED) $(CURSORS_ANIMATED) $(WINCURSORS_ANI) $(LWINCURSORS_ANI) $(WINCURSORS_LARGE_ANI) $(LWINCURSORS_LARGE_ANI); do \
			rm -f $${c/.ani/}-*.*.png; \
			rm -f $${c/.ani/}-*.*left.png; \
		done; \
	}
	rm -f $(WINCURSORS) $(LWINCURSORS) $(WINCURSORS_ANI) $(LWINCURSORS_ANI)
	rm -f $(WINCURSORS_LARGE) $(LWINCURSORS_LARGE) $(WINCURSORS_LARGE_ANI) $(LWINCURSORS_LARGE_ANI)
	rm -f *.in png2cur animaker
	rm -f *.32*.png *.48*.png *.64*.png *.96*.png *.128*.png
	rm -f *.cur
	rm -f inkscape-version
	test "x$(DARK_THEME)" = "x" && $(MAKE) DARK_THEME=1 clean || true

.PHONY: all all-dist all.left all.small all.medium all.large all.small.left all.medium.left all.large.left \
	clean dist dist.left dist.small dist.medium dist.large dist.small.left dist.medium.left dist.large.left \
	install preview source-dist theme theme.left theme.small theme.medium theme.large theme.small.left \
	theme.medium.left theme.large.left windows-cursors all-sizes all-themes wait.%.frames \
	progress.%.frames progress.left.%.frames wait_all_frames progress_all_frames \
	progress_left_all_frames wait_all_hotspots progress_all_hotspots progress_left_all_hotspots
.SUFFIXES:
.PRECIOUS: %.in %_left.in %.png %.left.png
.DELETE_ON_ERROR:
