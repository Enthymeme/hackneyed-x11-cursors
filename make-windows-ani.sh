#!/usr/bin/env bash
#
# Copyright (C) Richard Ferreira
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written authorization.

die()
{
	echo $@ >&2
	exit 1
}

make_source_list()
{
	for s in $SIZES; do
		for h in $hotspots; do
			custom_hotspots="${custom_hotspots} -s $h"
		done
		[ "$variant" ] && source_pngs="$source_pngs $@.$s.${variant}.png" ||\
                       source_pngs="$source_pngs $@.$s.png"
	done
}

while [ "$1" ]; do
	case "$1" in
	sizes=*)
		SIZES=${1#*=}
		shift ;;
	target=*)
		target=${1#*=}
		shift ;;
	frames=*)
		frames=${1#*=}
		shift ;;
	default_frametime=*)
		default_frametime=${1#*=}
		shift ;;
	frame_*_time=*)
		var=${1%=*}
		eval $var=${1#*=}
		shift ;;
	hotspots=*)
		hotspots=${1#*=}
		shift ;;
	output_ani=*)
		output_ani=${1#*=}
		shift ;;
	*)
		die "invalid parameter: $1"
	esac
done

# Windows apparently does not support animated cursors
# larger than 64px, in spite of their static cursors
# already going up to 128px
: ${SIZES:=24 48 64}
: ${default_frametime:=1}
: ${target:?missing target}
: ${frames:?missing frame count}
: ${output_ani:?no output file specified}
: ${hotspot_src:=theme/24/${target}.in}
variant=${target#*.}
[ "$variant" = "$target" ] && unset variant
if [ "$variant" ]; then
	target=${target%.*}
	hotspot_src=theme/24/${target}_${variant}.in
fi
if [ -e $hotspot_src ]; then
	set -- $(cut -d' ' -f2,3 $hotspot_src)
	hotspot_from_file="-s 32x32@${1},${2}"
fi
for ((i = 1; i <= frames; i++)); do
	make_source_list "${target}-${i}"
	[ "$variant" ] && output_cur="${target}-${i}.${variant}.cur" || output_cur="${target}-${i}.cur"
	./png2cur ${hotspot_from_file} ${custom_hotspots} -o $output_cur $source_pngs || die
	eval frametime=\$frame_${i}_time
	[ "$frametime" ] && output_cur="$output_cur=$frametime"
	cmdline="$cmdline $output_cur"
	unset source_pngs
done
./animaker -o ${output_ani} -t $default_frametime $cmdline
