#!/bin/bash
nprocs=$(getconf _NPROCESSORS_ONLN)
: ${nprocs:=4}
nprocs=$((nprocs+1))

dark_theme=1
while [ "$1" ]; do
	case "$1" in
	--with-windows-cursors)
		wincursors=windows-cursors
		shift
		;;
	--light-theme-only)
		dark_theme=0
		shift
		;;
	--dark-theme-only)
		dark_theme="only"
		shift
		;;
	*)
		shift
		;;
	esac
done

if [ "$dark_theme" != "only" ]; then
	make clean && make -O -j${nprocs} all-dist preview ${wincursors} || exit $?
fi
if [ "$dark_theme" = 1 -o "$dark_theme" = "only" ]; then
	make clean && make -O -j${nprocs} DARK_THEME=1 all-dist preview ${wincursors} || exit $?
fi
make clean
