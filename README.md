![hackneyed-light-preview](preview-Hackneyed.png "The sands of time are running out for you, bro")
![hackneyed-dark-preview](preview-Hackneyed-Dark.png "...PINK")

Overview
--------

Hackneyed is a scalable cursor theme mildly resembing old Windows 3.x/NT 3.x cursors. Dark and left-handed versions are also available, as well as an actual SVG version (supported only by KDE for now).


Building
--------
Minimum dependencies:

* ImageMagick (>=6.8.6, now also working with version 7 and later)
* Inkscape (>=0.92.3)
* GNU `make` and `bash`
* `xcursorgen` (part of `xorg-x11-apps`)
* [`jq`](https://jqlang.github.io/jq/) >= 1.7.1


To generate and pack Windows cursors (entirely optional):

* A working C compiler;
* `ImageMagick` >= 6.8.6;
* `libpng` >= 1.6.36;
* `pkg-config`;
* `zip`.

The generated Windows cursors employ an alpha channel and are compatible with Windows XP and later (Windows 10/11 users: they are high-DPI ready as well but see [Known bugs](#known-bugs)).

To ease the build process, a script called `build-all-themes.sh` is provided, accepting the following options:

* `--with-windows-cursors`: build Windows cursors as well (off by default);
* `--light-theme-only`: build the light theme only;
* `--dark-theme-only`: build the dark theme only.

Production targets (all of them generate tarballs for distribution):

* `dist`: build the scalable, right-handed theme;
* `dist.left`: build the scalable, left-handed theme;
* `dist.<size>`, where `size` can be small, medium or large: build a fixed-size, right-handed theme of the specified size;
* `dist.<size>.left`: build a fixed-size, left-handed theme of the specified size;
* `windows-cursors` for Windows.

`make all-dist` targets all of the above (except `windows-cursors`), including all fixed-size themes in all available sizes.

`small`, `medium` and `large` are currently defined as 24, 36 and 48, respectively. The making of individual cursors recognizes the integer argument only (see below).

The targets described below are meant for debugging and do not generate tarballs:

* `theme`: make the scalable, right-handed theme;
* `theme-svg`: make right-handed SVG theme;
* `theme.left`: make the scalable, right-handed theme;
* `theme-svg.left`: make left-handed SVG theme;
* `theme.<size>`: make a fixed-size, right-handed theme of the specified size;
* `theme.<size>.left`: make a fixed-size, left-handed theme of the specified size.

All of them run `do-symlinks.sh` when finished. The targets below do not:

* `all`: make the scalable, right-handed theme;
* `all.<size>`: make a fixed-size, right-handed theme of the specified size;
* `all.left`: make the scalable, left-handed theme;
* `all.<size>.left`: make a fixed-size, left-handed theme of the specified size.

Individual cursors can be made with `make <cursor_name>.<size in pixels>.<orientation>`, e.g.:

* `make default.24.left` for a left-handed, 24px `default` cursor;
* `make default.24` for a right-handed, 24px cursor;
* `make default.left` for a scalable, left-handed cursor;
* `make default.24.png` for a 24px PNG only. PNG sizes aren't hardcoded, so you can specify any size you want;
* or simply `make default` for a scalable, right-handed cursor.

Parallel jobs (`-j`) are recommended (Inkscape is _slow_).

If you don't feel like building it from source, grab the latet builds from the artwork page on [openDesktop.org](https://www.opendesktop.org/p/999998/) or from [Gitlab](https://gitlab.com/Enthymeme/hackneyed-x11-cursors/-/releases).

To build the dark theme, use `make DARK_THEME=1 <target>`.


Installation
------------
Simply choose a tarball and extract it to `$HOME/.icons`. If you've built it from source, pick a size or variant (e.g., `Hackneyed`, `LHackneyed-36px`) and move it to `$HOME/.icons`.

The install targets follow the logic above (`install`, `install.left`, `install.small.left` etc.), and there's also the `install-all` target for all themes, in all available sizes, colors and variants. The default prefix is `/usr/local`; change that by passing `PREFIX=<your prefix>` to `make`,
and `DESTDIR` to change the destination directory.


License
-------
Hackneyed is released under the MIT/X11 license.


Credits
-------
* do-symlinks.sh, rewrite of addmissing.sh, taken from [dummy-x11-cursors](https://www.opendesktop.org/p/999853/) by ultr

* `pencil` borrowed from Breeze

* monolithic SVG idea (and the SVG itself) taken from KDE's [Breeze theme](https://github.com/KDE/breeze/tree/master/cursors) by Ken Vermette

* `png2cur.c`, a bloated C rendition of [ico2cur.py](https://gist.github.com/RyanBalfanz/2371463)


Bugs/enhancements
-----------------
Suggestions and bug reports can be made here, or on openDesktop.

Windows-related feature requests, especially on the design front, are *always* no ETA/*sine die*, as this is first and foremost an X11/Wayland cursor theme. If you need a feature for Windows in a given timeframe, you submit a patch and become a maintainer.

Starting with 0.9.1, I'm taking packaging bugs into account. If you're packaging Hackneyed for your distro and it fails to build from source, report it to me, with or without a patch (but a patch will obviously speed things up).


A word about hashes
-------------------
libXcursor-based applications will return a hash for custom cursors when you run them
like `env XCURSOR_DISCOVER=1 <executable>` on a terminal. Take into account only the hashes
that return zero, e.g., "Cursor hash `0xdeadbeef` returns `0x0`".

(Forget about doing this to animated cursors: a hash is generated for every frame.)


Useful links
------------
* [Freedesktop.org's cursor specification](http://www.freedesktop.org/wiki/Specifications/cursor-spec/)

* [CSS cursors from W3C](http://dev.w3.org/csswg/css-ui/#propdef-cursor "2drafty4u")

* [Mozilla's test page for CSS cursors](https://developer.mozilla.org/en-US/docs/Web/CSS/cursor)

* [Qt requirements for X11 cursors](http://doc.qt.io/qt-5/qcursor.html#a-note-for-x11-users)

* [Core X11 cursors](http://tronche.com/gui/x/xlib/appendix/b/ "coffee_mug > all")


Setting a default theme for all applications
--------------------------------------------
Setting the cursor theme through a graphical interface doesn't always propagate to all applications. _Most of the time_, this can be solved by creating a file called `index.theme` with the following content...

```
[Icon Theme]
inherits=Hackneyed      # or Hackneyed-48px, LHackneyed etc.
```

...and saving it in `~/.icons/default` (create the folder if it doesn't exist already).

Chromium and Chromium-based browsers won't pick up a new theme unless you restart the browser (seems to be fixed in Chromium 80).

The above also fixes Flatpak apps defaulting to the system cursor, provided you symlink your `~/.icons` dir as [shown below](#known-bugs).


Known bugs
----------
The bugs described below are NotMyProblem<sup>TM</sup> and should be dealt with by upstream. Workarounds provided may or may not work.

- **GTK 3 and 4**. Versions older than 3.24.36 and 4.10.4, in what I assume was an oversight (although entirely on brand for GNOME), only look for cursor themes in `/usr/local/share/icons` or `/usr/share/icons`. If Flatpak is installed, then it searches whatever `<datadir>/flatpak/exports/share/icons` is available (either system-wide ones or in `~/.local/share/flatpak`). If you're affected by this, symlink your `~/.icons` dir to `~/.local/share/flatpak/exports/share/icons`.

- **Windows not loading cursors at logon**. Move the cursor files to `C:\Windows\Cursors`, preferably in a subfolder--I have no idea why this happens and I cannot be bothered to investigate. I've also not tested other locations outside `%USERPROFILE%` (e.g. `C:\Program Files`); feel free to experiment if you care. Fixed in the very first release of Windows 11 (21H2). Whether it will remain fixed is also not my problem.
	- Windows is also limited to 64x64 pixels for ANI cursors.

- **Plasma 6/KWin 6.0 displaying `all-scroll` instead of `move` when moving windows.** [Upstream bug.](https://bugs.kde.org/show_bug.cgi?id=483305) Fixed on Plasma 6.1.

