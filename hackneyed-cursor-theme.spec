Name:		hackneyed-cursor-theme	
Version:	0.9.3
Release:	1%{?dist}
Summary:	Windows 3.x-inspired cursors

Group:		Unspecified
License:	MIT
URL:		http://gitlab.com/Enthymem/hackneyed-x11-cursors
Source0:	hackneyed-x11-cursors-%{version}.tar.gz

BuildRequires:	xcursorgen inkscape ImageMagick make
BuildArch:	noarch

%description
Inspired by old Windows 3.x cursors, the Hackneyed cursor theme brings an
old-school feel to your wobbly-windowed, blocky and docky desktop of today.
Available in sizes 24, 36, 48, 60, 72, 84 and 96; a dark variant is also
available.

%prep
%setup -q -n hackneyed-x11-cursors-%{version}


%build


# ideally I'd have to build a subpackage for each variant
# but ideally I'd also have to write a Makefile that didn't suck major ass
# let's be real here
%install
%make_install %{?_smp_mflags} -B -O VERBOSE=1 PREFIX=/usr install.left
%make_install %{?_smp_mflags} -B -O VERBOSE=1 DARK_THEME=1 PREFIX=/usr install.left


%files
%doc README.md CHANGELOG LICENSE
%{_datadir}/icons/*Hackneyed
%{_datadir}/icons/*Hackneyed-Dark

%changelog
* Fri Sep 01 2023 Richard Ferreira <richardleoferreira@outlook.com> - 0.9-1
- Initial release


